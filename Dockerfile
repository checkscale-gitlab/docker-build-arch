FROM archlinux:base-devel
RUN pacman -Syu --noconfirm && \
    pacman -S --noconfirm git namcap && \
    mkdir /build || exit 0 && \
    useradd -G wheel -s /bin/bash -M -d /build build && \
    echo "build ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/build && \
    chown build:build /build
WORKDIR /build
