# Docker build image: Arch Linux

Docker image with Arch Linux + `base-devel` geared towards builds.
User `build`, with home `/build` can be used to run `makepkg`.

## Example

```plaintext
$ tree ./build
build
├── PKGBUILD
└── test
```

```plaintext
$ cat ./build/test
#!/usr/bin/env bash

pacman -S $(cat PKGBUILD | grep 'depends=' | grep -v 'optdepends=' | grep -v 'makedepends=' | sed 's/depends=(//g' | sed 's/)//g') --noconfirm
sudo -u build makepkg -si --noconfirm
```

```sh
docker run --rm -v $(pwd)/build:/build registry.gitlab.com/sjugge/docker/build-images/docker-build-arch /bin/bash -c "bash test"
```
